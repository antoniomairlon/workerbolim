using System;
using System.IO;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Mail;

namespace MonitoramentoSites
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        private string path;

        private readonly ServiceConfigurations _serviceConfigurations;

        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            path = @"c:\temp\MyTest2.txt";

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Criado");
                }
            }

            _serviceConfigurations = new ServiceConfigurations();
            new ConfigureFromConfigurationOptions<ServiceConfigurations>(
                configuration.GetSection("ServiceConfigurations"))
                    .Configure(_serviceConfigurations);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker executando em: {time}",
                    DateTimeOffset.Now);


                try
                {
                    //Envio de email
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

                    mail.From = new MailAddress("bolimoficialcotec@gmail.com");
                    mail.To.Add("mach.x@hotmail.com");
                    mail.Subject = "Test Mail";
                    mail.Body = "This is for testing SMTP mail from GMAIL";

                    SmtpServer.UseDefaultCredentials = false;

                    SmtpServer.Port = 587;
                    SmtpServer.Credentials = new System.Net.NetworkCredential("bolimoficialcotec@gmail.com", "Sandman7");
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                    //fim

                }
                catch (Exception ex)
                {
                    _logger.LogInformation(ex.ToString());
                }

                foreach (string host in _serviceConfigurations.Hosts)
                {
                    _logger.LogInformation(
                        $"Verificando a disponibilidade do host {host}");

                    string texto;

                    var resultado = new ResultadoMonitoramento();
                    resultado.Horario =
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    resultado.Host = host;

                    // Verifica a disponibilidade efetuando um ping
                    // no host que foi configurado em appsettings.json
                    try
                    {
                        using (Ping p = new Ping())
                        {
                            var resposta = p.Send(host);
                            resultado.Status = resposta.Status.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        resultado.Status = "Exception";
                        resultado.Exception = ex;
                    }

                    string jsonResultado =
                        JsonConvert.SerializeObject(resultado);
                    if (resultado.Exception == null)
                        _logger.LogInformation(jsonResultado);
                    else
                        _logger.LogError(jsonResultado);

                    texto = File.ReadAllText(path);

                    // Open the file to read from.
                    using (StreamWriter sw = new StreamWriter(path))
                    {
                        sw.WriteLine(texto + jsonResultado + " - " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Environment.NewLine);
                    }
                }
                await Task.Delay(_serviceConfigurations.Intervalo, stoppingToken);
            }
        }
    }
}